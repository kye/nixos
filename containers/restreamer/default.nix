/*
datarhei Restreamer provides real-time live video streaming on websites and social media platforms
without additional streaming provider costs for video hosting or software licensing costs.
*/
{
  config,
  lib,
  ...
}:
with lib; let
  cfg = config.cont.restreamer;
in {
  options.cont.restreamer = {
    #
    enable = mkOption {
      type = types.bool;
      default = false;
      example = true;
      description = "enable container";
    };
    #
    macvlanIp = mkOption {
      type = types.nullOr types.str;
      default = "";
      example = "10.10.10.1";
      description = "container macvlan ip address";
    };
    #
    vlanIp = mkOption {
      type = types.nullOr types.str;
      default = "";
      example = "12.12.12.1";
      description = "backend network ip address";
    };
    #
    contName = mkOption {
      type = types.str;
      default = "restreamer-${config.networking.hostName}";
      example = "container-cool-hostname";
      description = "container name, is also used for container volume dir name and activation script";
    };

    timeZone = mkOption {
      type = types.str;
      default = "Australia/Melbourne";
      example = "Australia/Broken_Hill";
      description = "database timezone";
    };
    #
    image = mkOption {
      type = types.str;
      default = "datarhei/restreamer:vaapi-latest";
      example = "datarhei/restreamer:cuda-latest";
      description = "container image";
    };
  };

  config = mkMerge [
    (mkIf (cfg.enable == true) {
      #
      system.activationScripts."make${cfg.contName}dir" =
        lib.stringAfter ["var"]
        ''mkdir -v -p /etc/oci.cont/${cfg.contName} & chown -R 1000:1000 /etc/oci.cont/${cfg.contName}'';

      environment.shellAliases = {cont-restreamer = "sudo podman pull ${cfg.image}";};

      virtualisation.oci-containers.containers.${cfg.contName} = {
        hostname = "${cfg.contName}";

        autoStart = true;

        image = "${cfg.image}";

        volumes = [
          "/etc/localtime:/etc/localtime:ro"

          "/etc/oci.cont/${cfg.contName}/config:/core/config"
          "/etc/oci.cont/${cfg.contName}/data:/core/data"
        ];

        environment = {
          TZ = "${cfg.timeZone}";
          PUID = "1000";
          PGID = "1000";
        };

        extraOptions = [
          "--privileged"
          "--network=macvlan_lan:ip=${cfg.macvlanIp}"
          # "--network=podman-backend:ip=${cfg.vlanIp}"
        ];
      };
    })
  ];
}
