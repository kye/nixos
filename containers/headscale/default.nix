{...}: {
  imports = [
    ./headscale.nix
    ./derp.nix
    ./ui.nix
  ];
}
