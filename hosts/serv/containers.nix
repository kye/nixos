{
  pkgs,
  config,
  secrets,
  ...
}: {
  imports = [
    ../../containers
    ../../containers/home-assistant
    ../../containers/plex
  ];

  virtualisation = {
    oci-containers.backend = "podman";
    podman = {
      enable = true;
      extraPackages = [pkgs.zfs];
      autoPrune.enable = true;
      dockerCompat = true;
      defaultNetwork.settings.dns_enabled = true;
    };
  };

  cont = {
    #
    backend-network = {
      enable = true;
      subnet = "${secrets.vlan.serv.subnet}";
      range = "${secrets.vlan.serv.range}";
      mask = "${secrets.vlan.serv.mask}";
    };
    #
    adguard = {
      enable = true;
      macvlanIp = "${secrets.ip.adguard-serv}";
      vlanIp = "${secrets.vlan.serv.adguard}";
      image = "adguard/adguardhome:latest";
      contName = "adguard-${config.networking.hostName}";
      timeZone = "Australia/Melbourne";
    };
    #
    immich = {
      enable = true;
      privateNetwork = false;
      macvlanDev = "";
    };
    #
    nzbget = {
      enable = true;
      macvlanIp = "${secrets.ip.nzbget}";
      vlanIp = "${secrets.vlan.serv.nzbget}";
    };
    #
    subgen = {
      enable = false;
      macvlanIp = "${secrets.ip.subgen}";
      vlanIp = "${secrets.vlan.serv.subgen}";
    };
    #
    subsai = {
      enable = false;
      macvlanIp = "${secrets.ip.subsai}";
      vlanIp = "${secrets.vlan.serv.subsai}";
    };
    #
    tailscale = {
      enable = true;
      macvlanIp = "${secrets.ip.tailscale-serv}";
      vlanIp = "${secrets.vlan.serv.tailscale}";
      vlanSubnet = "${secrets.vlan.serv.subnet}";
      image = "tailscale/tailscale:latest";
      subnet = "${secrets.ip.subnet}";
      contName = "tailscale-${config.networking.hostName}-subnet";
      authKey = "${secrets.password.tailscale}";
    };
  };

  systemd.services."podman-network-macvlan_lan" = {
    path = [pkgs.podman];
    wantedBy = [
      "podman-home-assistant.service"
      "podman-plex.service"
      "podman-adguard-${config.networking.hostName}.service"
      "podman-tailscale-${config.networking.hostName}-subnet.service"
      "podman-nzbget-${config.networking.hostName}.service"
    ];
    serviceConfig = {
      Type = "oneshot";
      RemainAfterExit = true;
      ExecStop = "${pkgs.podman}/bin/podman network rm -f macvlan_lan";
    };
    script = ''
      podman network exists macvlan_lan || podman network create --driver macvlan --opt parent=eno1 --subnet ${toString secrets.ip.subnet}/24 --ip-range ${toString secrets.ip.range}/24 --gateway ${toString secrets.ip.gateway} --disable-dns=false macvlan_lan
    '';
  };
}
